# Mass cytometry (CyTOF)
Data are extracted from R scripts. `relab` data are expressed in relative abundance either among all cells (`_allcells.csv`) either by subpopulation (e.g. `_bcells.csv`). `merge1` stores relative abundance data of main PBMC subpopulations (T cells, B cells, NK cells, Myeloid cells, unidentified). `statemk_by_typemk.csv` stores the relative abundance of state clusters among each phenotypical (type) metaclusters. These `.csv` files can be reproduced by running scripts of the CyTOF pipeline.

# Metabolomic (Mass spectrometry)
Data from dried pellets of white blood cells or plasma samples are stores in `*_cells` and `*_plasma` files.

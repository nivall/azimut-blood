#' Draw boxplots of main PBMC populations
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(ggplot2)
library(reshape2)
library(tidyverse)
library(scales)

#' Set path
path_to_git = "~/Git/azimut-blood"
path_to_save = "~/tmp"
output_name_stat = "stat_mainpop.txt"
output_name_plot = "plot_mainpop.pdf"

#' input
#' ## METADATA
setwd(path_to_git)
source("scripts/datamanagement/clinical.R")
## dat to plot
merge1 = read.csv("data/cytof/relab_merge1_allcells.csv") %>%     relocate(azimut_id)

#' prepare dataframes
##merge dataframes
df_plot = inner_join(merge1, md, by = "azimut_id")
## order groups
df_plot$group = factor(df_plot$group, c("HD", "PLA", "AZM") )

#' get the cols for the analysis
col_metaclusters = which(grepl("meta", colnames(df_plot), fixed = TRUE) | colnames(df_plot) == "group" | colnames(df_plot) == "azimut_id") 
df_plot_col = df_plot[col_metaclusters]
names(df_plot_col)[2:6] = c("B cells", "Myeloid cells", "NK cells", "T cells", "Unknown")

#' change dataset format to plot results
df_plot_col = melt(df_plot_col, id.vars = c("azimut_id","group") )
df_plot_col$variable = factor(df_plot_col$variable, levels = c("B cells", "T cells", "NK cells", "Myeloid cells", "Unknown") )

p = ggplot(df_plot_col,  aes(x = group , y =  value , fill = group ) )+
    #geom_violin ( ) +
    geom_boxplot( outlier.shape = NA, color = "black") +
    facet_wrap(   ~ variable, strip.position = "top", nrow = 1) +
    geom_jitter(shape = 21, fill = "white", color = "black", width = 0.1, size = 1)+
    theme_minimal()+
    ylab("% population") +
    theme(axis.text.x = element_blank(),
          axis.title.x = element_blank(),
          axis.title.y = element_text(color = "black", face = "bold", size = 12),
          axis.text.y = element_text(color = "black", face = "bold", size = 10),
          plot.title = element_text(size = 10, face = "bold"),
          strip.text.x = element_text( size = 10, color = "black", face = "bold"  ),
          axis.ticks.y = element_line(color = "black"),
          panel.grid.minor = element_blank(),
          panel.grid.major.x = element_blank(),
          axis.line = element_line(color = "black"),
          legend.position= "none" )+
    scale_fill_brewer(palette = "Dark2")+
    scale_color_brewer(palette="Dark2")

#' print results
setwd(path_to_save)
pdf(output_name_plot, height = 1.5, width = 7 )
print(p)
dev.off()


#' get the cols for the analysis
col_metaclusters = which(grepl("meta", colnames(df_plot), fixed = TRUE) | colnames(df_plot) == "group" | colnames(df_plot) == "azimut_id") 
df_plot_col = df_plot[col_metaclusters]
names(df_plot_col)[2:6] = c("B cells", "Myeloid cells", "NK cells", "T cells", "Unknown")
df_plot_test = df_plot_col %>% filter(group != "HD")
sink(output_name_stat)
for(i in 2:6){
    cat(names(df_plot_test)[i])
    print(wilcox.test(df_plot_test[ , i] ~ df_plot_test$group))
}

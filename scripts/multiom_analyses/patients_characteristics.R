#' Get patients characteristics and figures for time to sample
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(ggplot2)
library(tidyverse)
library(survminer)
library(cmprsk)
library(riskRegression)
library(prodlim)
library(UpSetR)
library(gtsummary)

#' Load multiomics datasets
source("~/Git/azimut-blood/scripts/multiom_analyses/import_datasets_for_multiomics.R")

#' Bit of datamanagement
md <- md %>%
    ## remove duplicated variables
    select(- c(dat_sample, del_sample_cytof, timesample_cytof) )

#' Set themes
theme_surv <- list(
    scale_color_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse")),
    scale_x_continuous(limits = c(0,24), breaks = seq(from = 0, to = 24, by = 3)),
    ylim(0,1),
    xlab("Time from HSCT (months)")
)

theme_boxplot <- list(
    #geom_boxplot(color = "black", size = 1, outlier.shape = NA),
    geom_violin(color = "black"),
    geom_jitter(shape = 21, fill = "white", height = 0.1),
    theme_minimal(),
    scale_fill_manual(values = c( "#D95F02", "#7570B3") ),
    scale_x_continuous(limits = c(0,200), breaks = seq(from = 0, to = 200, by = 25)),
    theme(panel.grid = element_blank(),
          axis.title.x = element_text(color = "black", size = 10),
          axis.title.y = element_blank(),
          axis.text    = element_text(color = "black", size = 10),
          axis.line    = element_line(color = "black"),
          axis.ticks   = element_line(color = "black")
          )
)

list_var <- c("age", "gender", "diagnosis", "status_transplant", "risk_transplant", "hsc_type", "conditioning", "hla", "gvhd_proph", "group", "relapse", "agvhd", "del_sample", "cmvDR", "cgvhd_grad", "bos") 

#' CyTOF data
data_cytof <- data_mk_raw %>%
    separate(azimut_id, into = c("allozithro_id", "dat_sample"), sep = "_" ) %>%
    left_join(md, by = "allozithro_id") %>%
    mutate(dat_sample = as.Date(dat_sample),
           group = fct_drop(.$group),
           group = fct_relevel(group, "PLA")) %>%
    add_column(del_sample = as.numeric(.$dat_sample - .$dat_hsct) )

cr_cytof <- cuminc(ftime = data_cytof$del_relapse, 
             fstatus = data_cytof$relapse, 
             cencode = "Censored",
             group = data_cytof$group)

mod_cytof <- FGR( Hist(del_relapse, relapse, cens.code = "Censored") ~ group,
               data = data_cytof, cause = "Relapse" ) %>%
    summary()

hr_cytof <- paste(mod_cytof$conf.int[1,1] %>% round(3),
                 mod_cytof$conf.int[1,3] %>% round(3),
                 mod_cytof$conf.int[1,4] %>% round(3),
                 sep = "-")

title <- paste0("cytof pval=",mod_cytof$coef[,"p-value"],
               " / HR=",hr_cytof)

surv_cytof <- ggcompetingrisks(fit = cr_cytof,
                              conf.int = TRUE,
                              #multiple_panels = FALSE,
                              ylab = "Cumulative incidence of event",
                              title = title) +
    scale_color_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    scale_fill_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    theme_surv

plot_cytof <- data_cytof %>%
    ggplot(aes(x = del_sample, y = group, fill = group) ) +
    theme_boxplot +
    ggtitle("cytof")

cytof_table <- data_cytof %>% select(all_of(list_var)) %>%
    tbl_summary(by = group) %>%
    add_p()

#' Cell metabolomic
data_cell <- cellmetabo_raw %>%
    separate(azimut_id, into = c("allozithro_id", "dat_sample"), sep = "_" ) %>%
    left_join(md, by = "allozithro_id") %>%
    mutate(dat_sample = as.Date(dat_sample),
           group = fct_drop(.$group),
           group = fct_relevel(group, "PLA")) %>%    
    add_column(del_sample = as.numeric(.$dat_sample - .$dat_hsct) )

cr_cell <- cuminc(ftime = data_cell$del_relapse, 
             fstatus = data_cell$relapse, 
             cencode = "Censored",
             group = data_cell$group)

mod_cell <- FGR( Hist(del_relapse, relapse, cens.code = "Censored") ~ group,
               data = data_cell, cause = "Relapse" ) %>%
    summary()

hr_cell <- paste(mod_cell$conf.int[1,1] %>% round(3),
                mod_cell$conf.int[1,3] %>% round(3),
                mod_cell$conf.int[1,4] %>% round(3),
                sep = "-")

title <- paste0("cell pval=", mod_cell$coef[,"p-value"],
               " / HR=",hr_cell)

surv_cell <- ggcompetingrisks(fit = cr_cell,
                              conf.int = TRUE,
                              #multiple_panels = FALSE,
                              ylab = "Cumulative incidence of event",
                              title = title) +
    scale_color_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    scale_fill_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    theme_surv

plot_cell <- data_cell %>%
    ggplot(aes(x = del_sample, y = group, fill = group) ) +
    theme_boxplot +
    ggtitle("cell")

cell_table <- data_cell %>% select(all_of(list_var)) %>%
    tbl_summary(by = group) %>%
    add_p()

#' Plasma metabolomic
data_plasma <- plasmetabo_raw %>%
    separate(azimut_id, into = c("allozithro_id", "dat_sample"), sep = "_" ) %>%
    left_join(md, by = "allozithro_id") %>%
    mutate(dat_sample = as.Date(dat_sample),
           group = fct_drop(.$group),
           group = fct_relevel(group, "PLA")) %>%
    add_column(del_sample = as.numeric(.$dat_sample - .$dat_hsct) )

cr_plasma <- cuminc(ftime = data_plasma$del_relapse, 
             fstatus = data_plasma$relapse,              cencode = "Censored",
             group = data_plasma$group)

mod_plasma <- FGR( Hist(del_relapse, relapse, cens.code = "Censored") ~ group,
               data = data_plasma, cause = "Relapse" ) %>%
    summary()

hr_plasma <- paste(mod_plasma$conf.int[1,1] %>% round(3),
                mod_plasma$conf.int[1,3] %>% round(3),
                mod_plasma$conf.int[1,4] %>% round(3),
                sep = "-")

title <- paste0("plasma pval=", mod_plasma$coef[,"p-value"],
               " / HR=",hr_plasma)

surv_plasma <- ggcompetingrisks(fit = cr_plasma,
                              conf.int = TRUE,
                              #multiple_panels = FALSE,
                              ylab = "Cumulative incidence of event",
                              title = title) +
    scale_color_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    scale_fill_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    theme_surv

plot_plasma <- data_plasma %>%
    ggplot(aes(x = del_sample, y = group, fill = group) ) +
    theme_boxplot +
    ggtitle("plasma")

plasma_table <- data_plasma %>% select(all_of(list_var)) %>%
    tbl_summary(by = group) %>%
    add_p()

#' Multiomic
data_multi <- azimutblood %>%
    #' drop rows with NA values
    drop_na() %>%
    #' reformat allozithro_id and merge with metadata
    separate(azimut_id, sep = "_", into = c("allozithro_id", "dat_sample")) %>%
    left_join(md, by = "allozithro_id") %>%
    mutate(dat_sample = as.Date(dat_sample),
           group = fct_drop(.$group),
           group = fct_relevel(group, "PLA")) %>%
    add_column(del_sample = as.numeric(.$dat_sample - .$dat_hsct) ) %>%
    #' remove non malignancy diagnosis
    filter(diagnosis != "Aplastic anemia")

cr_multi <- cuminc(ftime = data_multi$del_relapse, 
             fstatus = data_multi$relapse, 
             cencode = "Censored",
             group = data_multi$group)

mod_multi <- FGR( Hist(del_relapse, relapse, cens.code = "Censored") ~ group,
               data = data_multi, cause = "Relapse" ) %>%
    summary()

hr_multi <- paste(mod_multi$conf.int[1,1] %>% round(3),
                mod_multi$conf.int[1,3] %>% round(3),
                mod_multi$conf.int[1,4] %>% round(3),
                sep = "-")

title <- paste0("multi pval=", mod_multi$coef[,"p-value"],
               " / HR=",hr_multi)

surv_multi <- ggcompetingrisks(fit = cr_multi,
                              conf.int = TRUE,
                              #multiple_panels = FALSE,
                              ylab = "Cumulative incidence of event",
                              title = title) +
    scale_color_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    scale_fill_manual(name="", values=c("blue","red"),
                       labels=c("Death without relapse", "Relapse"))+
    theme_surv

plot_multi <- data_multi %>%
    ggplot(aes(x = del_sample, y = group, fill = group) ) +
    theme_boxplot +
    ggtitle("multi")

multi_table <- data_multi %>% select(all_of(list_var)) %>%
    tbl_summary(by = group) %>%
    add_p()

#' Extract data from different datasets
save_to_html <- function(table, file_name) {
    table %>%    # build gtsummary table
        as_gt() %>%             # convert to gt table
        gt::gtsave(             # save table as image
                filename = file_name
            )
}
save_to_html(cytof_table, "~/tmp/cytof_table.html")
save_to_html(cell_table, "~/tmp/cell_table.html")
save_to_html(plasma_table, "~/tmp/plasma_table.html")
save_to_html(multi_table, "~/tmp/multi_table.html")

#' Draw upset diagram
merge <- data_mk_raw %>%
    add_column(cytof = 1) %>%
    select(azimut_id, cytof) %>%
    full_join(cellmetabo_raw %>%
              add_column(cell = 1) %>%
              select(azimut_id, cell),
              by = "azimut_id") %>%
    full_join(plasmetabo_raw %>%
              add_column(plasma = 1) %>%
              select(azimut_id, plasma),
              by = "azimut_id") %>%
    replace_na(list(cytof = 0, cell = 0, plasma = 0)) %>%
    separate(azimut_id, into = c("allozithro_id", "dat_sample"), sep = "_" ) %>%
    left_join(md %>%
              select(allozithro_id, group),
              by = "allozithro_id") %>%
    #' remove patient with aplastic anemia
    filter(allozithro_id != "009-1105-H-H")


pdf("~/tmp/upset.pdf", width = 2, height = 3)
upset(merge %>%
      filter(group == "AZM"), sets = c("cytof", "plasma", "cell"), order.by = "freq",
      sets.x.label = "AZM cohort")
upset(merge %>%
      filter(group == "PLA"), sets = c("cytof", "plasma", "cell"), order.by = "freq",
      sets.x.label = "PLA cohort")
dev.off()


#' Print plots

pdf("~/tmp/time.pdf", width = 4, height = 1.5)
print(plot_cytof)
print(plot_cell)
print(plot_plasma)
print(plot_multi)
print(plot_overall)
dev.off()


pdf("~/tmp/survival.pdf", width = 10, height = 5)
print(surv_cytof)
print(surv_cell)
print(surv_plasma)
print(surv_multi)
print(surv_overall)
dev.off()

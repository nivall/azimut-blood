# CITE-Seq data from patients (`citeseq`)

This directory stores scripts and intermediate data to reproduce analyses from CITE-Seq experiments of ALLOZITHRO patients. All raw and processed files can be obtained from GEO (GSE208399).

## guixconfig
Contains files to load `GUIX` session in linux environment for CITE-Seq experiments analyses.

## Specific metadata

* Patients identificators for characteristic analysis (`samplesMetadata.csv`)
* Hallmark gene sets used for the analyses (`h.all.v7.5.1.symbols.gmt`)

## Pre-processing
The script `preprocess.R` transform filtered feature barcode matrix into a R object. Quality controls, transformation, normalization and clustering are applied during the process.

## Analyses and results

* Perform the pathway enrichment from Hallmark gene sets (`FGSEA.R`)
* Perform comparative analysis of PBMC subsets cell cycle (`boxplot_cell_cycle.R`)
* Print table of CITE-Seq patients characteristics (`citeseq_characteristics.R`)
* Perform differential expression analysis in all cell subsets (`diffexpression_adtclust.R`)
* Analysis of Class II HLA gene expression in cell subets (`figureGeneHLAII.R`)
* Script wrapper to load all used results from `diffexpr` folder (`load_CSV_file_diffexpr.R`)
* Draw a pie chart with number of cells from patients' groups (`pie_chart.R`)

## Outputs (`diffexpr`)

This directory stores intermediate results of differentially expressed genes among cell subsets.

# CyTOF analyses 

## Pipeline

### Preprocessing (`cytof_preprocess`)

* raw CyTOF files are normed with beads (`bead_based_normalization.R`)
* samples with multiple files are concatenated (`concatenate_fcsfiles.R`)
* cells are identified and .fcs is cleaned by manual gating on `FlowJO` see file `gatingstrategy.png`. These files can be accessed in the following repository [FR-FCM-Z5ZB](https://flowrepository.org/id/FR-FCM-Z5ZB)
* files are rename according to sample id and group AZM or PLA (`rename_files.R`)

### FlowSOM clustering with CATALYST (`cytof_flowsom`)
Here FCS files should be placed in `~/cytof_files`, metadata in `~/metadata`, output will be saved `~/R_output/sce_objects/`, `~/R_output/figures/`. 

* files are converted in SCE format (`importfcsdata.R`)
* metaclusters optimal number is chosen according to the results of metacluster screening (`flowsom_screen.R`)
* final FlowSOM is computed and appended to SCE object (`flowsom_final.R`)
* if needed metaclusters are merged (`flowsom_merge.R`)
* if needed check clustering with scatter plots (`flowsom_check_scatter.R`)

### UMAP visualisation of clustering (`cytof_umap`)

* UMAP are calculated, stored and exported as .pdf (`cytof_getumap.R`)
* print UMAPs according to clustering markers (`cytof_printumap_markers.R`)

### Creating sub sce objects of main subpopulations from all events (`cytof_subpopsce`)

* get SCE objects of subpopulations, run UMAP dimension reduction in subpopulations SCE objects, print UMAPs according to clustering markers in subpopulations SCE objects

### Analysis of T cells functionnal markers (`cytof_tcellfunct`)

Here FCS files should be placed in `~/cytof_files`, metadata in `~/metadata`, output will be saved `~/R_output/sce_objects/`, `~/R_output/figures/`.

* prepare the T cells sce object save type metacluster info (`cytof_func_prep_sce_tcells.R`) 
* screen different number of state metaclusters to define optimal number of metacluster to choose (`cytof_func_flowsom_screen.R`)
* final FlowSOM with T cells functionnal markers (`cytof_func_flowsom_valid.R`)
* if needed check clustering with scatter plots (`printscatter_funmk_merge.R`)

## Extract abundance (`cytof_extractdata`)

* from SCE extract relative abundances from various `SCE` objects.

## Perform statistical analyses (`cytof_analyses`)

* batch effect analysis: 1/ perform PCA analysis with mean/median antigen detection
* univariate analysis to compare immune cells populations from AZM and PLA patients (`univariate_mww.R`)
* univariate analysis to compare functionnal (state) populations from T cells of AZM and PLA patients (`univariate_mww_tcellfun.R`)

## Draw figures (`cytof_figures`)

* Draw boxplots of main PBMC cells (T cells, B cells, NK cells, Myeloid, unidentified) (`boxplot_mainpop_azmpla.R`)
* Draw boxplots of main metaclusters among main PBMC cells (`boxplot_mkbysubpop_fig.R`)
* Draw circular dendrogram of metaclusters hierarchy (`circulardendrogram.R`)
* Metadata to generate dendrogram (`data_circulardendogram.xlsx`)
* Draw UMAP figures (`plot_dimensionreductionfigures.R`)
* Draw dotplots of state cluster abundance among phenotypical clusters (`tcell_fun_dotplot_fig.R`)

# Metabolomics analyses and figures (`metabo_analyses`, `metabo_figures`)

* Pathway enrichment analyses (`enrichement.R`)
* Univariate analyses of metabolites (`metabo_univ_mww.R`)
* Draw donut plot of studied metabolites (`donut_plot_metabo.R`)
* Draw volcano plot from univariate analyses of metabolites (`volcano_mww.R`)
* Analysis of drugs levels correlations (`metabo_AZMinteraction.R`)
* Analysis of drugs detection in groups of patients (`metabo_drugsAZMgroup.R`)
* Batch analysis of metabolomic data (`pca_timeHSCT.R`)

# Integrative analysis (`multiom_analyses`)

* Draw chordiagram of inter-omics correlations (`chorddiagram.R`)
* Compute univariate competitive risk analyses of each studied variable (`cmprsk_univariate.R`)
* Datamanagement and various transformation to perform mutliomics analysis (`import_datasets_for_multiomics.R`)
* Multiomics analyses with principal component analysis (`multiomics_pca.R`)
* Get patients characteristics from each omic layer (`patients_characteristics.R`)
* Perform subgroup analyses with variables associated with AZM intake (`subgroup_analyses.R`)

# Datamanagement: perform task to prepare datasets for analyses (`datamanagment`)

* prepare clinical metadata (`clinical.R`)
* prepare CyTOF data with relative abundance according to main PBMC cells (`cytof_createdataframe_pop.R`)
* prepare intra cellular metabolomic data (`metabo_import_cells.R`)
* prepare drugs metabolomic data (`metabo_import_drugs.R`)
* prepare plasmatic metabolomic data (`metabo_import_plasma.R`)

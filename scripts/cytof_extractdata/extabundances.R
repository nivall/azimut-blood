## Extract abundance data from SCE object
## Project AZIMUT
## Nicolas Vallet 

## The purpose of this script is to extract cluster abundance from a single cell experiment object

## require
library(CATALYST)
library(SingleCellExperiment)
library(tidyverse)

#' remove scientific notation of decimals
options(scipen = 999)

#' Input : a SCE object with metaclusters and manually merged clusters
#' Output : .csv file

#' Replace the path of the SCE object you want to extract
message("Reading SCE object")
sce = readRDS("~/R_output/sce_objects/sce_all_clustmergumap.RData")
message("Done\n")


#' extract events and relative abundance
t = table(sce$sample_id, cluster_ids(sce, "meta55") ) # create a table of events par patients ids
prop = prop.table(t, 1)*100
res = as.data.frame.matrix(prop) # transform to dataframe
colnames(res) = paste0("meta",colnames(res)) # get the right col names
res$azimut_id = row.names(res) # get the variable names 
res = as_tibble(res) # change to tibble
res =  res %>% relocate(azimut_id)

#' save .csv file
write.csv(res, "~/tmp/relab_allcells.csv", row.names = FALSE )

#'-------- FROM SUBSET SCE --------'#

#' get data from subset SCE objects
list = c("tcells", "bcells", "nkcells", "myeloid")

for(i in list) {
 
    path_read = paste0("~/R_output/sce_objects/sce_",i,".RData")
    path_sav  = paste0("~/tmp/relab_",i,".csv") 

    message("read sce ", i )
    sce = readRDS(path_read)
    message("Done\n")

    #' extract events and relative abundance
    t = table(sce$sample_id, cluster_ids(sce, "meta55") ) # create a table of events par patients ids
    prop = prop.table(t, 1)*100
    res = as.data.frame.matrix(prop) # transform to dataframe
    colnames(res) = paste0("meta",colnames(res)) # get the right col names
    res$azimut_id = row.names(res) # get the variable names 
    res = as_tibble(res) # change to tibble
    res =  res %>% relocate(azimut_id)

    #' save .csv file
    write.csv(res, path_sav, row.names = FALSE  )
    
}




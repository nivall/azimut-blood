# Metadata files
Miscellaneous metadata :

- batch: date of sample preparation to study potential batch effect
- clinical: various clinical data
- cytof_sampledates: dates of sample collection
- metadata_files: info on samples used for CyTOF analyses with `CATALYST` package
- metadata_metabo_class: list of metabolites and their corresponding pathway and subpathway
- metadata_panel: info on panel used for CyTOF analyses with `CATALYST`
- mk_id55final: identification of CyTOF metaclusters

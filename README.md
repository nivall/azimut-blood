# Deciphering the impact of azithromycin on immune cells of patients included in the ALLOZITHRO study.

# Purpose
Relapse after allogeneic hematopoietic stem cell transplantation remains the first cause of death and understanding its mechanisms is an unmet clinical need. Azithromycin evaluated to prevent pulmonary graft-versus-host disease, was associated with relapse in a previous phase 3 randomized placebo-controlled trial. 

The aim of this repository is to store source codes and data used to analyse samples from patients included in the [ALLOZITHRO-study](https://clinicaltrials.gov/ct2/show/NCT01959100).

To be sure to reproduce easily the analyses make sure to clone in repo in `~/Git/` directory and to create the `~/tmp` directory for the ouputs. Some path may need to be changed within each scripts to make sure the inputs and outputs are found.

# Computing environment
To reproduce analyses, scripts can be ran in a [GNU Guix environment](https://guix.gnu.org/en/about/) with the `channels.scm` and `manifest.scm` files. Use the command below to run `R`.

```
guix time-machine -C channels.scm -- environment -m manifest.scm -- R
```

Additionnal informations on Guix environment can be found in the following ressources:

- [Our git repository](https://gitlab.com/nivall/guixreprodsci)
- [Guix HPC website](https://hpc.guix.info/)

# Repository
## data
Contains files with data from mass cytometry (CyTOF) assays or metabolomics to perform analyses. 

- CyTOF raw data can be downloaded in the repository: FlowRepository [FR-FCM-Z5ZB](https://flowrepository.org/id/FR-FCM-Z5ZB)
- Metabolomic raw data can be downloaded in the repository: Metabolights [MTBLS406](https://www.ebi.ac.uk/metabolights/MTBLS406)

## guixconfig
Contains files to load `GUIX` session in linux environment (for CITE-Seq experiments, files are in the dedicated folder).

## metadata
Contains metadata files which are used in scripts.

## scripts
Contains all scripts ordered according to omic data.

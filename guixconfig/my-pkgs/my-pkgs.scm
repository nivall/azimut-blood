;; build packages not indexed in guix

(define-module (my-pkgs)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bioconductor)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages guile)
  #:use-module (guix packages)
  #:use-module (guix build-system r)
  #:use-module (guix download)
  #:use-module (guix licenses)
  )

(define-public r-diffcyt
  (package
   (name "r-diffcyt")
   (version "1.10.0")
   (source
    (origin
     (method url-fetch)
     (uri (bioconductor-uri "diffcyt" version))
     (sha256
      (base32
       "068hv0r3lmyvi5ql2ijzbkm9h1n1xb9c8dlmy55syyxdmksfhpn9"))))
   (properties `((upstream-name . "diffcyt")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-circlize" ,r-circlize)
      ("r-complexheatmap" ,r-complexheatmap)
      ("r-dplyr" ,r-dplyr)
      ("r-edger" ,r-edger)
      ("r-flowcore" ,r-flowcore)
      ("r-flowsom" ,r-flowsom)
      ("r-limma" ,r-limma)
      ("r-lme4" ,r-lme4)
      ("r-magrittr" ,r-magrittr)
      ("r-multcomp" ,r-multcomp)
      ("r-reshape2" ,r-reshape2)
      ("r-s4vectors" ,r-s4vectors)
      ("r-summarizedexperiment"
       ,r-summarizedexperiment)
      ("r-tidyr" ,r-tidyr)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page "https://github.com/lmweber/diffcyt")
   (synopsis
    "Differential discovery in high-dimensional cytometry via high-resolution clustering")
   (description
    "Statistical methods for differential discovery analyses in high-dimensional cytometry data (including flow cytometry, mass cytometry or CyTOF, and oligonucleotide-tagged cytometry), based on a combination of high-resolution clustering and empirical Bayes moderated tests adapted from transcriptomics.")
   (license expat)) )

(define-public r-publish
  (package
   (name "r-publish")
   (version "2020.12.23")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "Publish" version))
     (sha256
      (base32
       "1qpv5hj9agmc4hrpskqk0lns8bh8w3j27d4ckh5y7gh1532qzad7"))))
   (properties `((upstream-name . "Publish")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-data-table" ,r-data-table)
      ("r-lava" ,r-lava)
      ("r-multcomp" ,r-multcomp)
      ("r-prodlim" ,r-prodlim)
      ("r-survival" ,r-survival)))
   (home-page
    "https://cran.r-project.org/package=Publish")
   (synopsis
    "Format Output of Various Routines in a Suitable Way for Reports and Publication")
   (description
    "This package provides a bunch of convenience functions that transform the results of some basic statistical analyses into table format nearly ready for publication.  This includes descriptive tables, tables of logistic regression and Cox regression results as well as forest plots.")
   (license gpl2+)) )

(define-public r-timereg
  (package
   (name "r-timereg")
   (version "1.9.8")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "timereg" version))
     (sha256
      (base32
       "065vlsgs5xm11w13837jdvi9jxfa6j2vxpcp2r6hgrs38xv01ch0"))))
   (properties `((upstream-name . "timereg")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-lava" ,r-lava)
      ("r-numderiv" ,r-numderiv)
      ("r-survival" ,r-survival)))
   (home-page "https://github.com/scheike/timereg")
   (synopsis
    "Flexible Regression Models for Survival Data")
   (description
    "Programs for Martinussen and Scheike (2006), `Dynamic Regression Models for Survival Data', Springer Verlag.  Plus more recent developments.  Additive survival model, semiparametric proportional odds model, fast cumulative residuals, excess risk models and more.  Flexible competing risks regression including GOF-tests.  Two-stage frailty modelling.  PLS for the additive risk model.  Lasso in the 'ahaz' package.")
   (license gpl2+)) )


(define-public r-mets
  (package
   (name "r-mets")
   (version "1.2.8.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "mets" version))
     (sha256
      (base32
       "0prhnjqgrvw650f89sy7wj52wsd7h7gc4iqinv17j54ag8xrnhcq"))))
   (properties `((upstream-name . "mets")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-lava" ,r-lava)
      ("r-mvtnorm" ,r-mvtnorm)
      ("r-numderiv" ,r-numderiv)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)
      ("r-survival" ,r-survival)
      ("r-timereg" ,r-timereg)))
   (native-inputs
    `(("gfortran" ,gfortran) ("r-knitr" ,r-knitr)))
   (home-page "https://kkholst.github.io/mets/")
   (synopsis "Analysis of Multivariate Event Times")
   (description
    "Implementation of various statistical models for multivariate event history data <doi:10.1007/s10985-013-9244-x>.  Including multivariate cumulative incidence models <doi:10.1002/sim.6016>, and  bivariate random effects probit models (Liability models) <doi:10.1016/j.csda.2015.01.014>.  Also contains two-stage binomial modelling that can do pairwise odds-ratio dependence modelling based marginal logistic regression models.  This is an alternative to the alternating logistic regression approach (ALR).")
   (license gpl2+)) )

(define-public r-riskregression
  (package
   (name "r-riskregression")
   (version "2020.12.08")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "riskRegression" version))
     (sha256
      (base32
       "0xa603fcpjd8kgcn1ldwzqpll7h6x738lknjjb5gbly84wbhv6aw"))))
   (properties
    `((upstream-name . "riskRegression")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-cmprsk" ,r-cmprsk)
      ("r-data-table" ,r-data-table)
      ("r-doparallel" ,r-doparallel)
      ("r-foreach" ,r-foreach)
      ("r-ggplot2" ,r-ggplot2)
      ("r-lattice" ,r-lattice)
      ("r-lava" ,r-lava)
      ("r-mets" ,r-mets)
      ("r-mvtnorm" ,r-mvtnorm)
      ("r-plotrix" ,r-plotrix)
      ("r-prodlim" ,r-prodlim)
      ("r-publish" ,r-publish)
      ("r-ranger" ,r-ranger)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)
      ("r-rms" ,r-rms)
      ("r-survival" ,r-survival)
      ("r-timereg" ,r-timereg)))
   (home-page
    "https://github.com/tagteam/riskRegression")
   (synopsis
    "Risk Regression Models and Prediction Scores for Survival Analysis with Competing Risks")
   (description
    "Implementation of the following methods for event history analysis.  Risk regression models for survival endpoints also in the presence of competing risks are fitted using binomial regression based on a time sequence of binary event status variables.  A formula interface for the Fine-Gray regression model and an interface for the combination of cause-specific Cox regression models.  A toolbox for assessing and comparing performance of risk predictions (risk markers and risk prediction models).  Prediction performance is measured by the Brier score and the area under the ROC curve for binary possibly time-dependent outcome.  Inverse probability of censoring weighting and pseudo values are used to deal with right censored data.  Lists of risk markers and lists of risk models are assessed simultaneously.  Cross-validation repeatedly splits the data, trains the risk prediction models on one part of each split and then summarizes and compares the performance across splits.")
   (license gpl2+)) )

#!/bin/sh

#  runguix.sh
#
#  Created by Nicolas Vallet on 06/01/2021.
#  

guix time-machine -C channels.scm -- environment -m manifest.scm -L my-pkgs -- R
